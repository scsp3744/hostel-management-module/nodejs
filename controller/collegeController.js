const express = require('express');
var router = express.Router();
var ObjectId = require('mongoose').Types.ObjectId;

var { College } = require('../models/college');

router.get('/',(req,res)=>{
    College.find((err, docs) => {
        if(!err){ res.send(docs); }
        else{
            console.log('Error in Retriving Colleges : '+ JSON.stringify(err, undefined, 2));
        }
    });
});

router.get('/:id', (req,res)=>{
    if(!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    College.findById(req.params.id,(err,doc)=>{
        if(!err){ res.send(doc); }
        else{
            console.log('Error in Retriving Colleges : '+ JSON.stringify(err, undefined, 2));
        }
    });
});

router.post('/',(req,res) => {
    var kolej = new College({
        name: req.body.name,
        address: req.body.address
    });
    kolej.save((err,doc)=>{
        if(!err){ res.send(doc); }
        else{
            console.log('Error in College Save : '+ JSON.stringify(err, undefined, 2));
        }
    });
});

router.put('/:id',(req,res)=>{
    if(!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);
    var kolej = {
        name: req.body.name,
        address: req.body.address
    };
    College.findByIdAndUpdate(req.params.id,
        {$set:kolej},
        {new:true},
        (err,doc)=>{
            if(!err){ res.send(doc); }
            else{
                console.log('Error in College Update : '+ JSON.stringify(err, undefined, 2));
            } 
        });
});

router.delete('/:id', (req,res) => {
    if(!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);
    College.findByIdAndRemove(req.params.id, (err, doc) =>{
        if(!err){ res.send(doc); }
            else{
                console.log('Error in College Delete : '+ JSON.stringify(err, undefined, 2));
            }
    });
});

module.exports = router;