const express = require('express');
var router = express.Router();
var ObjectId = require('mongoose').Types.ObjectId;

var { Block } = require('../models/block');

router.get('/',(req,res)=>{
    Block.find((err, docs) => {
        if(!err){ res.send(docs); }
        else{
            console.log('Error in Retriving Blocks : '+ JSON.stringify(err, undefined, 2));
        }
    });
});

router.get('/:id', (req,res)=>{
    if(!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    Block.findById(req.params.id,(err,doc)=>{
        if(!err){ res.send(doc); }
        else{
            console.log('Error in Retriving Block : '+ JSON.stringify(err, undefined, 2));
        }
    });
});

router.post('/',(req,res) => {
    var blok = new Block({
        college: req.body.college,
        name: req.body.name
    });
    blok.save((err,doc)=>{
        if(!err){ res.send(doc); }
        else{
            console.log('Error in Block Save : '+ JSON.stringify(err, undefined, 2));
        }
    });
});

router.put('/:id',(req,res)=>{
    if(!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);
    var blok = {
        college: req.body.college,
        name: req.body.name
    };
    Block.findByIdAndUpdate(req.params.id,
        {$set:blok},
        {new:true},
        (err,doc)=>{
            if(!err){ res.send(doc); }
            else{
                console.log('Error in Block Update : '+ JSON.stringify(err, undefined, 2));
            } 
        });
});

router.delete('/:id', (req,res) => {
    if(!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);
    Block.findByIdAndRemove(req.params.id, (err, doc) =>{
        if(!err){ res.send(doc); }
            else{
                console.log('Error in Block Delete : '+ JSON.stringify(err, undefined, 2));
            }
    });
});

module.exports = router;